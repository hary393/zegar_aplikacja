﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Clock
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {


        DateTime stopWatchTime;
        TimeSpan stopWatchResume = new TimeSpan(0, 0, 0);
        TimeSpan stopWatchCurrent = new TimeSpan(0, 0, 0);
        TimeSpan countdownTime;
        DispatcherTimer timer = new DispatcherTimer();
        DispatcherTimer stopwatchTimer = new DispatcherTimer();
        DispatcherTimer countdownTimer = new DispatcherTimer();
        public MainWindow()
        {
            InitializeComponent();
           
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += digital_Timer;
            timer.Start();
            stopwatchTimer.Interval = TimeSpan.FromSeconds(1);
            stopwatchTimer.Tick += digital_Stopwatch;
            countdownTimer.Interval = TimeSpan.FromSeconds(1);
            countdownTimer.Tick += digital_Countdown;
        }

        void digital_Timer(object sender, EventArgs e)
        {
            label4Clock.Content = DateTime.Now.ToLongTimeString();
            double milsec = DateTime.Now.Millisecond;
            double sec = DateTime.Now.Second;
            double min = DateTime.Now.Minute;
            double hr = DateTime.Now.Hour;
            seconds.LayoutTransform = new RotateTransform(((sec / 60) * 360) + ((milsec / 1000) * 6));
            minutes.LayoutTransform = new RotateTransform((min * 360 / 60) + ((sec / 60) * 6));
            hours.LayoutTransform = new RotateTransform((hr * 360 / 12) + (min / 2));
        }
        void digital_Stopwatch(object sender, EventArgs e)
        {
            stopWatchCurrent = DateTime.Now - stopWatchTime + stopWatchResume;
            label5Stopwatch.Content = stopWatchCurrent.ToString(@"hh\:mm\:ss");
        }
        void digital_Countdown(object sender, EventArgs e)
        {
            countdownTime -= new TimeSpan(0, 0, 1);
            label3Countdown.Content = countdownTime.ToString(@"hh\:mm\:ss");
            if (countdownTime.Hours==0 && countdownTime.Minutes==0 && countdownTime.Seconds==0)
            {
                countdownTimer.Stop();
                SystemSounds.Beep.Play();
                //MessageBox.Show("Countdown Complete", "Alert",MessageBoxButton.OK, MessageBoxImage.None, MessageBoxResult.None,MessageBoxOptions.DefaultDesktopOnly);
                OKMessage messagewindow = new OKMessage();
                messagewindow.Show();
                messagewindow.Activate();
            }
        }


        private void button_Countdown(object sender, RoutedEventArgs e)
        {
            int hours, minutes, seconds;
            Int32.TryParse(textBox.Text.ToString(), out hours);
            Int32.TryParse(textBox_Copy.Text.ToString(), out minutes);
            Int32.TryParse(textBox_Copy1.Text.ToString(), out seconds);
            countdownTime = new TimeSpan(hours, minutes, seconds);
            label3Countdown.Content = countdownTime.ToString(@"hh\:mm\:ss");
            if (!countdownTimer.IsEnabled)
            {
                countdownTimer.Start();
            }
        }

        private void button1_ClockType(object sender, RoutedEventArgs e)
        {
            if (buttonType.Content.Equals("Analog"))
            {
                buttonType.Content = "Digital";
                label4Clock.Visibility = Visibility.Hidden;
                seconds.Visibility = Visibility.Visible;
                hours.Visibility = Visibility.Visible;
                minutes.Visibility = Visibility.Visible;
            }
            else
            {
                buttonType.Content = "Analog";
                label4Clock.Visibility = Visibility.Visible;
                seconds.Visibility = Visibility.Hidden;
                hours.Visibility = Visibility.Hidden;
                minutes.Visibility = Visibility.Hidden;
            }
            //buttonType.Content = (buttonType.Content.Equals("Analog")) ? "Digital" : "Analog";
            
        }

        private void button2_Stopwatch_Start(object sender, RoutedEventArgs e)
        {
            if (!stopwatchTimer.IsEnabled)
            {
                stopWatchTime = DateTime.Now;
                stopwatchTimer.Start();
            }
        }

        private void button3_StopStopwatch(object sender, RoutedEventArgs e)
        {
            stopwatchTimer.Stop();
            stopWatchResume = stopWatchCurrent;
        }

        private void button4_ResetStopwatch(object sender, RoutedEventArgs e)
        {
            stopWatchResume = new TimeSpan(0, 0, 0);
            stopWatchCurrent = new TimeSpan(0, 0, 0);
            stopWatchTime = DateTime.Now;
            label5Stopwatch.Content = "00:00:00";
        }

        private void NumberValidation(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
